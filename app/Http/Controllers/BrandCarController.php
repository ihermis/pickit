<?php

namespace App\Http\Controllers;

use App\Models\Brand;

class BrandCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        return response($brands, 200)
            ->header('Content-Type', 'text/json');
    }

}
