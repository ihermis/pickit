<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\CarService;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::with(['brand', 'model', 'owner'])->withCount('services')->get();
        return response($cars, 200)
            ->header('Content-Type', 'text/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $car = new Car();
        $car->year = $request->year;
        $car->patent = $request->patent;
        $car->color = $request->color;
        $car->brand_id = $request->brand;
        $car->owner_id = $request->owner;
        $car->model_id = $request->model;

        if ($car->save()) {
            \Log::channel('pickit')->info('Auto registrado! | ' . json_encode($request->all()));
            return response('Auto registrado!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }
    }

    /**
     * Associate services with a customer's cars
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addBudget(Request $request)
    {

        $data = [];
        foreach ($request->presupuesto as $servicio) {
            $data[] = [
                'car_id' => $servicio['car_id'],
                'service_id' => $servicio['service_id'],
                'subtotal' => $servicio['cost'],
            ];
        }

        if (CarService::insert($data)) {
            \Log::channel('pickit')->info('Servicios asociados! | ' . json_encode($request->all()));
            return response('Servicios asociados!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $owner = Car::where(['id' => $id])->with(['brand', 'model', 'owner', 'services'])->first();
        return response($owner, 200)->header('Content-Type', 'text/json');
    }

    /**
     * Display  a listing of the resource by owner id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function carByOwner($id)
    {
        $owner = Car::where(['owner_id' => $id])->with(['brand', 'model', 'services'])->get();
        return response($owner, 200)->header('Content-Type', 'text/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $car = Car::find($id);
        $car->year = $request->year;
        $car->patent = $request->patent;
        $car->color = $request->year;
        $car->brand_id = $request->brand;
        $car->owner_id = $request->owner;
        $car->model_id = $request->model;

        if ($car->save()) {
            \Log::channel('pickit')->info('Datos del auto actualizado! | ' . json_encode($request->all()));
            return response('Datos del auto actualizad!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $car = Car::find($id);
            if ($car->delete()) {
                \Log::channel('pickit')->info('Cliente Eliminado! | ' . json_encode($car));
                return response('Cliente Eliminado!', 200)->header('Content-Type', 'text/json');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            $car = Car::where(['id' => $id])->withCount('services')->first();

            if ($car->cars_count) {
                return response('El vehículo tiene servicios asociados, no se puede eliminar.', 400)->header('Content-Type', 'text/json');
            } else {
                return response('Ha ocurrido un error al intentar eliminar los datos.', 400)->header('Content-Type', 'text/json');
            }

        }
    }
}
