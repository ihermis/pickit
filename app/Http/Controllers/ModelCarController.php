<?php

namespace App\Http\Controllers;

use App\Models\ModelCar;

class ModelCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = ModelCar::all();
        return response($models, 200)
            ->header('Content-Type', 'text/json');
    }

    /**
     * Display the specified resource.
     *
     * @param int  $brandId
     * @return \Illuminate\Http\Response
     */
    public function show($brandId)
    {
        $model = ModelCar::where(['brand_id' => $brandId])->get();
        return response($model, 200)
            ->header('Content-Type', 'text/json');
    }
}
