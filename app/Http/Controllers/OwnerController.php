<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $owners = Owner::withCount('cars')->get();
        return response($owners, 200)
            ->header('Content-Type', 'text/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $owner = new Owner();
        $owner->firstname = $request->input('firstname');
        $owner->lastname = $request->input('lastname');

        $owner->save();

        return response('Data save!', 200)
            ->header('Content-Type', 'text/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $OwnerId
     * @return \Illuminate\Http\Response
     */
    public function show($OwnerId)
    {
        $owner = Owner::find($OwnerId);
        return response($owner, 200)->header('Content-Type', 'text/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $OwnerId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $OwnerId)
    {
        $owner = Owner::find($OwnerId);
        $owner->firstname = $request->input('firstname');
        $owner->lastname = $request->input('lastname');

        if ($owner->save()) {
            \Log::channel('pickit')->info('Datos actualizados! | ' . json_encode($request->all()));
            return response('Datos actualizados!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $OwnerId
     * @return \Illuminate\Http\Response
     */
    public function destroy($OwnerId)
    {
        try {
            $owner = Owner::find($OwnerId);
            if ($owner->delete()) {
                \Log::channel('pickit')->info('Cliente Eliminado! | ' . json_encode($owner));
                return response('Cliente Eliminado!', 200)->header('Content-Type', 'text/json');
            }

        } catch (\Illuminate\Database\QueryException $e) {
            $owner = Owner::where(['id' => $OwnerId])->withCount('cars')->first();

            if ($owner->cars_count) {
                return response('El Cliente tiene vehículos asociados, no se puede eliminar.', 400)->header('Content-Type', 'text/json');
            } else {
                return response('Ha ocurrido un error al intentar eliminar los datos.', 400)->header('Content-Type', 'text/json');
            }

        }
    }
}
