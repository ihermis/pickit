<?php

namespace App\Http\Controllers;

use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::all();
        return response($service, 200)
            ->header('Content-Type', 'text/json');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new Service();
        $service->name = $request->name;
        $service->cost = $request->cost;

        if ($service->save()) {
            \Log::channel('pickit')->info('Servicio registrado! | ' . json_encode($request->all()));
            return response('Servicio registrado!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        return response($service, 200)
            ->header('Content-Type', 'text/json');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $service->name = $request->name;
        $service->cost = $request->cost;

        if ($service->save()) {
            \Log::channel('pickit')->info('Servicio actualizado! | ' . json_encode($request->all()));
            return response('Servicio actualizado!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);

        if ($service->delete()) {
            \Log::channel('pickit')->info('Servicio eliminado!');
            return response('Servicio eliminado!', 200)->header('Content-Type', 'text/json');
        } else {
            return response('Ha ocurrido un error al procesar los datos. Verifica que los datos sean correctos', 400)->header('Content-Type', 'text/json');
        }
    }
}
