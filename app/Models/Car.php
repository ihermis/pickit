<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    public function services()
    {
        return $this->belongsToMany(Service::class , 'car_services', 'car_id' , 'service_id')->withPivot('subtotal' , 'created_at');
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function model()
    {
        return $this->belongsTo(ModelCar::class);
    }

}
