<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Car;
class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Car();
        $user->year = "2020";
        $user->patent  = "XCV-2323";
        $user->color = "Rojo";
        $user->brand_id  = 1;
        $user->owner_id = 1;
        $user->model_id  = 1;
        $user->save();

        $user = new Car();
        $user->year = "2019";
        $user->patent  = "XCV-2222";
        $user->color = "Amarillo";
        $user->brand_id  = 1;
        $user->owner_id = 1;
        $user->model_id  = 2;
        $user->save();
    }
}
