<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserSeeder::class);
        $this->call(BrandCarSeeder::class);
        $this->call(ModelCarSeeder::class);
        $this->call(OwnerSeeder::class);
        $this->call(CarSeeder::class);
        $this->call(ServiceSeeder::class);
    }
}
