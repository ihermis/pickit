<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Owner;
class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Owner();
        $user->firstname = "Juan";
        $user->lastname  = "Perez";
        $user->save();

        $user = new Owner();
        $user->firstname = "Jose";
        $user->lastname  = "Perez";
        $user->save();
    }
}
