<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Service;
class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                'name' => 'Cambio de Aceite',
                'cost' => 100
            ],
            [
                'name' => 'Cambio de Filtro',
                'cost' => 200
            ],
            [
                'name' => 'Cambio de Correa',
                'cost' => 115.50
            ],
            [
                'name' => 'Revisión General',
                'cost' => 400
            ]
        );

    Service::insert($data);

    }
}
