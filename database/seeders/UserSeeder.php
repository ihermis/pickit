<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //\DB::delete('DELETE FROM users;');
        $user = new User();
        $user->name = "Hermis";
        $user->email  = "ihermis@gmail.com";
        $user->password = bcrypt("123123");
        $user->role = 'admin';
        $user->save();

        $user = new User();
        $user->name = "Usuario Común";
        $user->email  = "usuario1@gmail.com";
        $user->password = bcrypt("123123");
        $user->role = 'comun';
        $user->save();
    }
}
