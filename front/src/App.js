
import './App.css';
import { BrowserRouter , Routes , Route, Navigate } from "react-router-dom";
import Header from './components/header';
import Home from './pages/home'
import ListCars from './pages/cars/ListCars'
import ListOwner from './pages/owner/ListOwner'
import ListServices from './pages/service/ListServices';
import FormOwner from './pages/owner/FormOwner'
import FormCar from './pages/cars/FormCar';
import FormServices from './pages/service/FormServices';
import FormBudgets from './pages/budgets/FromBudgets';
import OwnerHistory from './pages/owner/OwnerHistory';
import Details from './pages/cars/Details';


function App() {
  return (
    <BrowserRouter>
    <Header/>
    <Routes>
        <Route path="/" element={<Home />} />
        
        <Route path="/clientes" element={<ListOwner />} />
        <Route path="/clientes/crear" element={<FormOwner />} />
        <Route path="/clientes/:id" element={<FormOwner />} />
        <Route path="/clientes/historial/:id" element={<OwnerHistory />} />

        <Route path="/autos" element={<ListCars />} />
        <Route path="/autos/crear" element={<FormCar />} />
        <Route path="/autos/:id" element={<FormCar />} />
        <Route path="/autos/detalles/:id" element={<Details />} />

        <Route path="/servicios" element={<ListServices />} />
        <Route path="/servicios/crear" element={<FormServices />} />
        <Route path="/servicios/:id" element={<FormServices />} />

        <Route path="/presupuestos" element={<FormBudgets />} />

        {/* <Route path="about" element={<About />} /> */}
        <Route path="*"  element={<Navigate to="/" />} />
      </Routes>
   
    </BrowserRouter>
  );
}

export default App;
