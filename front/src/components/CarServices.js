import React, { useEffect ,useState } from 'react'
import { URL_API } from '../utils/constants';
import axios from 'axios';
import Services from './Services';
const CarServices = (props) => {
    const [showTable,setShowTable] = useState(true);
    const [carOwner,setCarOwner] = useState([]);
    const [carDetail,setCarDetail] = useState(false);
    
    useEffect(()=>{
        //console.log(props);
        setShowTable(props.show);

        if(props.ownerId){
            axios.get(URL_API + 'autos/clientes/' + props.ownerId)
            .then(function (response) {
                
                if(response.status === 200){
                    setCarOwner(response.data);
                }               
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
        }
        if(props.carId){
            axios.get(URL_API + 'autos/' + props.carId)
            .then(function (response) {
                console.log(response.data);
                if(response.status === 200){                           
                    setCarOwner(array => [...array, response.data]);
                    setCarDetail(true);
                    
                }               
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
        }
    },[props]);

    return (
        <>
            {
                showTable ? (
                    <div className='container'>

                        {
                            carOwner.length > 0 ? (
                                carOwner.map((car,index)=> 
                                        <div  key={index}> 
                                            <p className='text-car'>
                                                <strong>Patente: </strong> {car.patent} <br />
                                                <strong>Año: </strong> {car.year} <br />
                                                <strong>Marca / Modelo: </strong> {car.brand.name} - {car.model.name} <br />
                                                <strong>Color: </strong> {car.color} <br />
                                                {
                                                    carDetail ? (<>
                                                        <strong>Propietario: </strong> {car.owner.firstname} {car.owner.lastname}<br />
                                                    </>) : ('')
                                                }
                                            </p>
                                            <Services array={car.services} show={true} />
                                        </div>
                                ) 
                            ) : (
                                <div className='no-service'>No tiene auto asociados. <a href='/autos'>Agregar un auto</a> </div>
                            )
                        }
                        
                    </div>
                ) : (
                 <></>
                )
            }
        </>      
    )
}

export default CarServices