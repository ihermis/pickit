import React, { useEffect ,useState } from 'react'

const Preloader = (props) => {
    const [showPreload,setShowPreload] = useState(true);
    useEffect(()=>{
        //console.log(props);
        setShowPreload(props.show);
    },[props]);

    return (
        <>
            {
                showPreload ? (
                    <div className='d-flex justify-content-center preload'>
                        <div className="spinner-border text-info" role="status">
                            <span className="sr-only"></span>
                        </div>
                    </div>
                ) : (
                 <></>
                )
            }
        </>      
    )
}

export default Preloader