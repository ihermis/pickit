import React, { useEffect ,useState } from 'react'

const Services = (props) => {
    const [services,setServices] = useState([]);
    const [total,setTotal] = useState(0);
    useEffect(()=>{
        //console.log(props);
        setServices(props.array);
        
    },[props]);

    const calcularTotal = () => {
        let totalTmp = parseFloat(0);
        services.map((service) => {
            totalTmp += parseFloat(service.pivot.subtotal);
        })

       return  totalTmp.toFixed(2);
        //const totalTmp = parseFloat(total) + parseFloat(cost);
        //console.log(totalTmp);
        //setTotal(totalTmp);
    }


    return (
        <>
        {
        services.length > 0 ? (
            <table className="table">
                <thead className="thead-light">
                    <tr>
                    <th scope="col">Fecha</th>
                    <th scope="col">Servicio</th>
                        <th scope="col">Costo</th>
                        
                    </tr>
                </thead>
                <tbody>
                    {
                        services.map((service,index)=>
                        <tr key={index}>
                            <td>{service.pivot.created_at}</td>
                            <td>{service.name}</td>
                            <td>{service.pivot.subtotal}</td>
                            
                        </tr>
                        )
                    }
                    <tr key={services.length + 1}>   
                        <td><strong>Total: </strong> </td>
                        <td></td>
                        <td><strong>{calcularTotal()}</strong></td>
                    </tr>
                    
                </tbody>
                </table>
            ) : (
                <div className='no-service'>No tiene servicios aplicados. <a href='/presupuestos'>Agregar servicios acá</a></div>
            )
        }
        </>
    )
}

export default Services