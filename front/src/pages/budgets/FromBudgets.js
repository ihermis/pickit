import React, { useEffect, useState } from 'react';
import { URL_API } from '../../utils/constants';

import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
  } from "react-router-dom";

import Preloader  from '../../components/Preloader';

const FormBudgets = () => {
    let navigate = useNavigate();
    const params = useParams();
    const [title,setTitle] = useState("");
    const [showForm,setShowForm] = useState(false);
    const [showPreload,setShowPreload] = useState(true);

    const [servicioId,setServicioId] = useState(0);
    const [servicios,setServicios] = useState([]);
    const [ownerId,setOwnerId] = useState(0);
    const [owners,setOwners] = useState([]);
    const [autoId,setAutoId] = useState(0);
    const [autos,setAutos] = useState([]);

    const [isUpdate,setIsUpdate] = useState(false);
    const [endpoint,setEndpoint] = useState('');

    const [serviciosAuto,setServiciosAuto] = useState([]);
    const [presupuesto,setPresupuesto] = useState([]);
    const [totalCosto,setTotalCosto] = useState(0);

    useEffect(() => {
        getServicios();
        getPropietarios();
        
        if(params.id){
            setIsUpdate(true);
            const url = 'presupuestos/' + params.id;
            setEndpoint(url);
            setTitle("Editar servicio");           
        }else{
            setTitle("Nuevo presupuesto");
            setEndpoint('presupuestos');
            setIsUpdate(false);
        }

        setTimeout(()=>{
            setShowPreload(false);
            setShowForm(true);
        },500);
        
       // sendForm();
       
      },[params]);

    useEffect(() => {
        getAutos();       
    },[ownerId]);
    
    const getAutos = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'autos/clientes/' + ownerId)
        .then(function (response) {

          setAutos([]);
          const data = response.data;
          
          for (let i = 0; i < data.length; i++) {
            const element = data[i];
            
            setAutos(array => [...array, element]);
          }
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }



    const agregarServicio = () => {
     
        const serviciosSelect = servicios[servicioId];
        
        const auto_index = autos.findIndex(auto => auto.id === parseInt(autoId));
        const dataPresupuesto = {
            service_id : serviciosSelect.id,
            car_id : parseInt(autoId),
            cost : serviciosSelect.cost,
            auto_info_tmp: autos[parseInt(auto_index)].patent + ' | ' + autos[parseInt(auto_index)].brand.name+ ' | ' + autos[parseInt(auto_index)].model.name,
            servicio_info_tmp : servicios[parseInt(servicioId)].name 
        };
   

        setPresupuesto(array => [...array, dataPresupuesto]);
        const suma = parseFloat(totalCosto) + parseFloat(serviciosSelect.cost);
        setTotalCosto(suma);
        setServicioId(servicioId);
   
    };
    
    const getServicios = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'servicios')
        .then(function (response) {
          setServicios([]);
          const data = response.data;
          
          for (let i = 0; i < data.length; i++) {
            const element = data[i];
            
            setServicios(array => [...array, element]);
          }
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }

    const getPropietarios = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'clientes')
        .then(function (response) {
          // handle success
          
          setOwners(response.data);
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }
    const sendForm = async () => {
        
        setShowPreload(true);
        await axios.post(URL_API + 'autos/agregar-presupuesto', {
            presupuesto : presupuesto
        })        
        .then(function (response) {
          // handle success
          setShowPreload(false);
          if(response.status === 200){
          
            alert(response.data);
            setTimeout(()=>{
                navigate('/clientes');
            },500);
          }
            
        })
        .catch(function (error) {
          // handle error
          setShowPreload(false);
            if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
            if(error.response.status === 500)
                alert('Hay un problema con el servidor, volvé a intentar.');
        });
        return false;
    }

    const eliminarServicio = (index) => {
        
        const presupuestoCopy = [...presupuesto]; 
        const total = parseFloat(totalCosto) - parseFloat(presupuesto[index].cost);
        setTotalCosto(total);
        
        presupuestoCopy.splice(index, 1);
        setPresupuesto(presupuestoCopy); 
    }

    return (
        <>
            
            <Preloader show={showPreload} />
            {
                showForm ? (
                    <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/autos" >Listado de autos</a></li>
                            <li className="breadcrumb-item active" aria-current="page">{title}</li>
                        </ol>
                    </nav>                        
                    <h1>{title}</h1>
                    <div className="row">
                        <div className="col-md-6">
                        <form>
                       
                       <div className="mb-3">
                           <label htmlFor="clientes" className="form-label">Clientes</label>
                           <select
                               className="form-control"
                               value={ownerId}
                               onChange={(e) =>{ setOwnerId(e.target.value)}}
                               id='clientes'
                           >
                               <option value='0'>selecciona</option>
                               {
                                   owners.map((owner , index) => {
                                       return <option value={owner.id} key={index}>{owner.firstname} {owner.lastname}</option>
                                   })
                               }
                           </select>
                       </div>

                       {
                           autos.length > 0 ? (
                            <div className="mb-3">
                                <label htmlFor="autos" className="form-label">Autos</label>
                                <select
                                    className="form-control"
                                    value={autoId}
                                    onChange={(e) =>{ setAutoId(e.target.value)}}
                                    id='autos'
                                >
                                    <option value='0'>selecciona</option>
                                    {
                                        autos.map((auto , index) => {
                                            return <option value={auto.id} key={index}>{auto.patent} | {auto.brand.name} | {auto.model.name}</option>
                                        })
                                    }
                                </select>
                            </div>
                           ) : (<></>)
                       }

{
                           (autoId !== 0 && servicios.length > 0)? (
                            <div className="mb-3">
                                <label htmlFor="servicios" className="form-label">Servicios</label>
                                <select
                                    className="form-control"
                                    onChange={(e) =>{ setServicioId(e.target.value)}}
                                    id='servicios'
                                >
                                    
                                    {
                                        servicios.map((servicio , index) => {
                                            return <option value={index} key={index}>{servicio.name} | ${servicio.cost}</option>
                                        })
                                    }
                                </select>
                            </div>
                           ) : (<></>)
                       }
                       
                   
                       <div className="mb-3">
                           <button type="button"  onClick={()=>{agregarServicio()}} className="btn btn-primary">Agregar Servicio</button>
                           {
                               presupuesto.length > 0 ? (
                                <button type="button" onClick={()=>{sendForm()}} className="btn btn-success">Registrar</button>
                               ) : ('')
                           }
                           
                           <a href="/autos" className="btn btn-secondary">Cancelar</a>
                       </div>
                   </form>
                        </div>
                    <div className="col-md-6 col_resumen">
                    <h5>Resumen</h5>
                    <table className='table table-striped'>
                    <thead className='thead-light'>
                        <tr>
                            <th>Auto</th>
                            <th>Servicio</th>
                            <th>Costo</th>
                            <th></th>
                        </tr>
                    </thead>
                        <tbody>
                            {
                                presupuesto.map((servicio , index) => 
                                <tr key={index}>                                    
                                    <td >{servicio.auto_info_tmp}</td>
                                    <td >{servicio.servicio_info_tmp}</td>
                                    <td >{servicio.cost}</td>
                                    <td >
                                        <button type="button" className="btn btn-danger" onClick={()=> eliminarServicio(index)}>X</button>
                                    </td>
                                </tr>
                                )
                            }
                        </tbody>
                        </table>
                        <div className='totalCosto'>
                            Total: {totalCosto}
                        </div>
                    </div>
                       
                    </div>
                    
                    </div>
                ) : (<></>)
            }
           
        </>
    );
}

export default FormBudgets;