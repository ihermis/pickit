import React, { useEffect ,useState } from 'react'
import { useParams } from 'react-router-dom';
import Preloader from '../../components/Preloader';
import CarServices from '../../components/CarServices';
const Details = () => {
    const params = useParams();
    const [carId,setCarId] = useState(0);
    const [servicios,setServicios] = useState([]);
    const title = 'Detalles del Auto';
    const [showPreload,setShowPreload] = useState(true);

    useEffect(()=>{
        
        setCarId(params.id);
        setTimeout(()=>{            
            setShowPreload(false);
        },500);
        
    },[params]);




    return (
        <>
            <div className='container'>
             <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="/autos" >Listado de autos</a></li>
                    <li className="breadcrumb-item active" aria-current="page">{title}</li>
                </ol>
            </nav>     
            <h1>{title}</h1>
            <Preloader show={showPreload} />
            {
                !showPreload ? (
                    <CarServices carId={carId} show={true}/>
                ) : ''
            }
            
        </div>
        </>
    )
}

export default Details