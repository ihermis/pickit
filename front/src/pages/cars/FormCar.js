import React, { useEffect, useState } from 'react';
import { URL_API } from '../../utils/constants';

import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
  } from "react-router-dom";

import Preloader  from '../../components/Preloader';

const FormCar = () => {
    let navigate = useNavigate();
    const params = useParams();
    const [title,setTitle] = useState("");
    const [showForm,setShowForm] = useState(false);
    const [showPreload,setShowPreload] = useState(true);

    const [brand,setBrand] = useState(0);
    const [arrayBrand,setArrayBrand] = useState([]);

    const [model,setModel] = useState(0);
    const [arrayModel,setArrayModel] = useState([]);

    const [year,setYear] = useState(0);
    const [patent,setPatent] = useState('');
    const [color,setColor] = useState('');

    const [owner,setOwner] = useState(0);
    const [arrayOwner,setArrayOwner] = useState(0);

    const [isUpdate,setIsUpdate] = useState(false);
    const [endpoint,setEndpoint] = useState('');

    useEffect(() => {
        getModelos(brand);
     },[brand]);

    useEffect(() => {
        getMarcas();
        getModelos();
        getPropietarios();

        if(params.id){
            setIsUpdate(true);
            const url = 'autos/actualizar/' + params.id;
            setEndpoint(url);
            setTitle("Editar auto");
            

            axios.get(URL_API + 'autos/' + params.id)
            .then(function (response) {
                
                if(response.status === 200){
                    setYear(response.data.year);
                    setPatent(response.data.patent);
                    
                    setModel(response.data.model_id);
                    setColor(response.data.color);
                    setOwner(response.data.owner_id);

                    setTimeout(()=>{
                        setBrand(response.data.brand_id);
                    },500);
                }               
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
            
        }else{
            setTitle("Registar nuevo auto");
            setEndpoint('autos/guardar');
            setIsUpdate(false);
        }

        setTimeout(()=>{
            setShowPreload(false);
            setShowForm(true);
        },500);
        
       // sendForm();

      
      },[params]);
    
      
    const getMarcas = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'marcas')        
        .then(function (response) {
            setArrayBrand(response.data);  
            setShowPreload(false);         
        })
        .catch(function (error) {
            // handle error
            setShowPreload(false);
            if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
            if(error.response.status === 500)
                alert('Hay un problema con el servidor, volvé a intentar.');
        });
    }

    

    const getModelos = async (brand) => {
        setShowPreload(true);
        let urlModel = 'modelos';
        if(brand)
            urlModel = 'modelos/' + brand;
            
        await axios.get(URL_API + urlModel)        
        .then(function (response) {
            setArrayModel(response.data);   
            setShowPreload(false);         
        })
        .catch(function (error) {
            // handle error
            setShowPreload(false);
            if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
            if(error.response.status === 500)
                alert('Hay un problema con el servidor, volvé a intentar.');
        });
    }
    
    


    const getPropietarios = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'clientes')
        .then(function (response) {
          // handle success
          
          setArrayOwner(response.data);
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }

    const sendForm = async () => {
        //event.preventDefault();
        
        if(brand === 0){
            alert('Seleccioná una marca de auto');
            return;
        }

        if(model === 0){
            alert('Seleccioná un modelo');
            return;
        }

        if(year === '' || year === 0){
            alert('Ingresá año el auto.');
            return;
        }

        if(year <= 1950 || year >= 2022){
            alert('El año del auto es inválido.');
            return;
        }

        if(patent === ''){
            alert('Ingresá la patente del auto.');
            return;
        }

        if(color === ''){
            alert('Ingresá el color del auto');
            return;
        }

        if(owner === 0){
            alert('Asigna un propietario.');
            return;
        }



        let data = {
            brand: brand,
            model: model,
            patent : patent,
            owner : owner,
            year : year,
            color : color
        };

        if(isUpdate){
            data._method = 'PUT';
        }
        
        
        setShowPreload(true);
        await axios.post(URL_API + endpoint, data)        
        .then(function (response) {
          // handle success
          setShowPreload(false);
          if(response.status === 200){
            alert(response.data);
            setTimeout(()=>{
                navigate('/autos');
            },500);
          }
            
        })
        .catch(function (error) {
          // handle error
          setShowPreload(false);
            if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
            if(error.response.status === 500)
                alert('Hay un problema con el servidor, volvé a intentar.');
        });
        return false;
    }

    return (
        <>
            
            <Preloader show={showPreload} />
            {
                showForm ? (
                    <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/autos" >Listado de autos</a></li>
                            <li className="breadcrumb-item active" aria-current="page">{title}</li>
                        </ol>
                    </nav>                        
                    <h1>{title}</h1>
                    <form>
                       
                            <div className="mb-3">
                                <label htmlFor="year" className="form-label">Año</label>
                                <input type="number" className="form-control" id="year" name="year" placeholder="" value={year} onChange={(e) =>{ setYear(e.target.value)}} />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="patent" className="form-label">Patente</label>
                                <input type="text" className="form-control" id="patent" name="patent" placeholder=""  value={patent} onChange={(e) =>{ setPatent(e.target.value)}}/>
                            </div>

                            <div className="mb-3">
                                <label htmlFor="color" className="form-label">Color</label>
                                <input type="text" className="form-control" id="color" name="color" placeholder=""  value={color} onChange={(e) =>{ setColor(e.target.value)}}/>
                            </div>

                            <div className="mb-3">
                                <label htmlFor="brand" className="form-label">Marca</label>                                

                                <select
                                    className="form-control"
                                    value={brand}
                                    onChange={(e) =>{ setBrand(e.target.value)}}
                                >
                                    <option value='0'>selecciona</option>
                                    {
                                        arrayBrand.map((brand , index) => {
                                            return <option value={brand.id} key={index}>{brand.name}</option>
                                        })
                                    }
                                </select>
                            </div>

                            <div className="mb-3">
                                <label htmlFor="model" className="form-label">Modelo</label>                                

                                <select
                                    className="form-control"
                                    value={model}
                                    onChange={(e) =>{ setModel(e.target.value)}}
                                >
                                    <option value='0'>selecciona</option>
                                    {
                                        arrayModel.map((model , index) => {
                                            return <option value={model.id} key={index}>{model.name}</option>
                                        })
                                    }
                                </select>
                            </div>

                            <div className="mb-3">
                                <label htmlFor="model" className="form-label">Propietario</label>                                

                                <select
                                    className="form-control"
                                    value={owner}
                                    onChange={(e) =>{ setOwner(e.target.value)}}
                                >
                                    <option value='0'>selecciona</option>
                                    {
                                        arrayOwner.map((owner , index) => {
                                            return <option value={owner.id} key={index}>{owner.firstname} {owner.lastname}</option>
                                        })
                                    }
                                </select>
                            </div>
                        
                            <div className="mb-3">
                                <button type="button"  onClick={()=>{sendForm()}} className="btn btn-primary">Guardar</button>
                                <a href="/autos" className="btn btn-secondary">Cancelar</a>
                            </div>
                        </form>
                    </div>
                ) : (<></>)
            }
           
        </>
    );
}

export default FormCar;