import React, { useState , useEffect } from 'react';
import axios from 'axios';
import {URL_API} from '../../utils/constants';
import Preloader  from '../../components/Preloader';


const ListCars = () => {    
    const [showPreload,setShowPreload] = useState(false);
    useEffect(() => {
        getAutos();

      },[]);
    const [autos,setAutos] = useState([]);


    const getAutos = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'autos')
        .then(function (response) {
          // handle success
          console.log(response);
          setAutos(response.data);
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }


    const eliminarRegistro = async (id , index) => {
        if(window.confirm("Se procederá a eliminar el auto y servicios")){
            await axios.delete(URL_API + 'autos/eliminar/' + id)
            .then(function (response) {
                if(response.status === 200){
                    const autosCopy = [...autos]; 
                    alert(response.data);
                    autosCopy.splice(index, 1);
                    setAutos(autosCopy);  
                }
                //console.log(response);
                //getAutos(response.data);
            })
            .catch(function (error) {
                if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
                if(error.response.status === 500)
                    alert('Hay un problema con el servidor, volvé a intentar.');
            });
        }
    }

    return (
        <div className='container'>
            <div className="card">
                <div className="card-header">
                    Autos <a href='/autos/crear' type="button" className="btn btn-success">+</a>
                </div>
                <div className="card-body">
                <Preloader show={showPreload} />
                <table className='table table-striped'>
                    <thead className='thead-light'>
                        <tr>
                            <th>ID</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Patente</th>
                            <th>Color</th>
                            <th>Propietario</th>
                            <th>Servicios</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            autos.map((auto , index) => 
                            <tr key={index}>
                                <td >{auto.id}</td>
                                <td >{auto.brand.name}</td>
                                <td >{auto.model.name}</td>
                                <td >{auto.patent}</td>
                                <td >{auto.color}</td>
                                <td >{auto.owner.firstname} {auto.owner.lastname}</td>
                                <td >{auto.services_count}</td>
                                <td >
                                    <a href={'/autos/detalles/' + auto.id }  type="button" className="btn btn-primary">Ver</a>
                                    <a href={'/autos/' + auto.id }  type="button" className="btn btn-warning">Editar</a>
                                    <button type="button" className="btn btn-danger" onClick={()=> eliminarRegistro(auto.id , index)}>Eliminar</button>
                                </td>
                            </tr>
                            )
                        }
                    </tbody>
                        </table>
                </div>
                </div>
                
        </div>
    )
}

export default ListCars