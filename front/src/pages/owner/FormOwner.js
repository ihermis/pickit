import React, { useEffect, useState } from 'react';
import { URL_API } from '../../utils/constants';

import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
  } from "react-router-dom";

import Preloader  from '../../components/Preloader';

const FormOwner = () => {
    let navigate = useNavigate();
    const params = useParams();
    const [title,setTitle] = useState("");
    const [showForm,setShowForm] = useState(false);
    const [showPreload,setShowPreload] = useState(true);
    const [firstname,setFirstname] = useState('');
    const [lastname,setLastname] = useState('');
    const [isUpdate,setIsUpdate] = useState(false);
    const [endpoint,setEndpoint] = useState('');

    useEffect(() => {

        if(params.id){
            setIsUpdate(true);
            const url = 'clientes/actualizar/' + params.id;
            setEndpoint(url);
            setTitle("Editar cliente");
            

            axios.get(URL_API + 'clientes/' + params.id)
            .then(function (response) {
                if(response.status === 200){
                    setFirstname(response.data.firstname);
                    setLastname(response.data.lastname);
                }               
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
            
        }else{
            setTitle("Nuevo cliente");
            setEndpoint('clientes/guardar');
            setIsUpdate(false);
        }

        setTimeout(()=>{
            setShowPreload(false);
            setShowForm(true);
        },500);
        
       // sendForm();
       
      },[params]);
    
    
    
    const sendForm = async () => {
        //event.preventDefault();
       
        if(firstname === ''){
            alert('Ingresá un nombre.');
            return;
        }

        if(lastname === ''){
            alert('Ingresá un apellido.');
            return;
        }

        let data = {
            firstname: firstname,
            lastname: lastname,
        };

        if(isUpdate){
            data._method = 'PUT';
        }
        
        setShowPreload(true);
        await axios.post(URL_API + endpoint, data)        
        .then(function (response) {
          // handle success
          setShowPreload(false);
          if(response.status === 200){
            setFirstname('');
            setLastname('');
            alert(response.data);
            setTimeout(()=>{
                navigate('/clientes');
            },500);
          }
            
        })
        .catch(function (error) {
          // handle error
          setShowPreload(false);
            if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
            if(error.response.status === 500)
                alert('Hay un problema con el servidor, volvé a intentar.');
        });
        return false;
    }

    return (
        <>
            
            <Preloader show={showPreload} />
            {
                showForm ? (
                    <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/clientes" >Listado de clientes</a></li>
                            <li className="breadcrumb-item active" aria-current="page">{title}</li>
                        </ol>
                    </nav>                        
                    <h1>{title}</h1>
                    <form>
                       
                            <div className="mb-3">
                                <label htmlFor="nombre" className="form-label">Nombre</label>
                                <input type="text" className="form-control" id="nombre" name="firstname" placeholder="Juan" value={firstname} onChange={(e) =>{ setFirstname(e.target.value)}} required/>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="apellido" className="form-label">Apellido</label>
                                <input type="text" className="form-control" id="apellido" name="lastname" placeholder="Perez"  value={lastname} onChange={(e) =>{ setLastname(e.target.value)}} required/>
                            </div>
                        
                            <div className="mb-3">
                                <button type="button"  onClick={()=>{sendForm()}} className="btn btn-primary">Enviar</button>
                                <a href="/clientes" className="btn btn-secondary">Cancelar</a>
                            </div>
                        </form>
                    </div>
                ) : (<></>)
            }
           
        </>
    );
}

export default FormOwner;