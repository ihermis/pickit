import React, { useState , useEffect } from 'react';
import axios from 'axios';
import {URL_API} from '../../utils/constants';
import Preloader  from '../../components/Preloader';
const ListOwner = () => {    
    const [showPreload,setShowPreload] = useState(false);
    useEffect(() => {
        getPropietarios();

      },[]);
    const [propietarios,setPropietarios] = useState([]);


    const getPropietarios = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'clientes')
        .then(function (response) {
          // handle success
          console.log(response);
          setPropietarios(response.data);
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }


    const eliminarRegistro = async (id , index) => {
        if(window.confirm("Se procederá a eliminar el propietario")){
            await axios.delete(URL_API + 'clientes/eliminar/' + id)
            .then(function (response) {
                if(response.status === 200){
                    const propietariosCopy = [...propietarios]; 
                    alert(response.data);
                    propietariosCopy.splice(index, 1);
                    setPropietarios(propietariosCopy);  
                }
                //console.log(response);
                //getPropietarios(response.data);
            })
            .catch(function (error) {
                if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
                if(error.response.status === 500)
                    alert('Hay un problema con el servidor, volvé a intentar.');
            });
        }
    }

    return (
        <div className='container'>
            <div className="card">
                <div className="card-header">
                    Clientes <a href='/clientes/crear' type="button" className="btn btn-success">+</a>
                </div>
                <div className="card-body">
                <Preloader show={showPreload} />
                <table className='table table-striped'>
                    <thead className='thead-light'>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>N° Autos</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            propietarios.map((propietario , index) => 
                            <tr key={index}>
                                <td >{propietario.id}</td>
                                <td >{propietario.firstname}</td>
                                <td >{propietario.lastname}</td>
                                <td >{propietario.cars_count}</td>
                                <td >
                                    <a href={'/clientes/historial/' + propietario.id }  type="button" className="btn btn-primary">Ver</a>
                                    <a href={'/clientes/' + propietario.id }  type="button" className="btn btn-warning">Editar</a>
                                    <button type="button" className="btn btn-danger" onClick={()=> eliminarRegistro(propietario.id , index)}>Eliminar</button>
                                </td>
                            </tr>
                            )
                        }
                    </tbody>
                        </table>
                </div>
                </div>
        </div>
    )
}

export default ListOwner