import React, { useEffect ,useState } from 'react'
import { useParams } from 'react-router-dom';
import CarServices from '../../components/CarServices';
import Preloader from '../../components/Preloader';

const OwnerHistory = (props) => {
    const params = useParams();
    const [ownerId,setOwnerId] = useState(0);
    const [showPreload,setShowPreload] = useState(true);
    const title = 'Historial de Automóviles';
    useEffect(()=>{        
        setOwnerId(params.id);
        setTimeout(()=>{
            
            setShowPreload(false);
        },500);
        
    },[params]);

    return (
        <div className='container'>
             <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="/clientes" >Listado de clientes</a></li>
                    <li className="breadcrumb-item active" aria-current="page">{title}</li>
                </ol>
            </nav>     
            <h1>{title}</h1>
            <Preloader show={showPreload} />
            {
                !showPreload ? (
                    <CarServices ownerId={ownerId} show={true}/>
                ) : ''
            }
            
        </div>
    )
}

export default OwnerHistory