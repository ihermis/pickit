import React, { useEffect, useState } from 'react';
import { URL_API } from '../../utils/constants';

import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import {
    useParams
  } from "react-router-dom";

import Preloader  from '../../components/Preloader';

const FormServices = () => {
    let navigate = useNavigate();
    const params = useParams();
    const [title,setTitle] = useState("");
    const [showForm,setShowForm] = useState(false);
    const [showPreload,setShowPreload] = useState(true);

    const [servicio,setServicio] = useState('');
    const [costo,setCosto] = useState(0);

    const [isUpdate,setIsUpdate] = useState(false);
    const [endpoint,setEndpoint] = useState('');

    useEffect(() => {

        if(params.id){
            setIsUpdate(true);
            const url = 'servicios/actualizar/' + params.id;
            setEndpoint(url);
            setTitle("Editar servicio");

            axios.get(URL_API + 'servicios/' + params.id)
            .then(function (response) {
                if(response.status === 200){
                    setServicio(response.data.name);
                    setCosto(response.data.cost);
                }               
            })
            .catch(function (error) {
                console.log(error);
            });
            
        }else{
            setTitle("Nuevo servicio");
            setEndpoint('servicios/guardar');
            setIsUpdate(false);
        }

        setTimeout(()=>{
            setShowPreload(false);
            setShowForm(true);
        },500);
        
       // sendForm();
       
      },[params]);
    
    
    
    const sendForm = async () => {
        //event.preventDefault();
        
        if(servicio === ''){
            alert('Ingresá un nombre para el servicio.');
            return;
        }

        if(costo === 0){
            alert('Ingresá precio del servicio.');
            return;
        }

        let data = {
            name: servicio,
            cost: costo,
        };

        if(isUpdate){
            data._method = 'PUT';
        }
        
        setShowPreload(true);
        await axios.post(URL_API + endpoint, data)        
        .then(function (response) {
          // handle success
          setShowPreload(false);
          if(response.status === 200){
            setServicio('');
            setCosto('');
            alert(response.data);
            setTimeout(()=>{
                navigate('/servicios');
            },500);
          }
            
        })
        .catch(function (error) {
          // handle error
          setShowPreload(false);
            if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
            if(error.response.status === 500)
                alert('Hay un problema con el servidor, volvé a intentar.');
        });
        return false;
    }

    return (
        <>
            
            <Preloader show={showPreload} />
            {
                showForm ? (
                    <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/servicios" >Listado de Servicios</a></li>
                            <li className="breadcrumb-item active" aria-current="page">{title}</li>
                        </ol>
                    </nav>                        
                    <h1>{title}</h1>
                    <form>
                       
                            <div className="mb-3">
                                <label htmlFor="servicio" className="form-label">Servicio</label>
                                <input type="text" className="form-control" id="servicio" name="servicio" placeholder="" value={servicio} onChange={(e) =>{ setServicio(e.target.value)}} required/>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="costo" className="form-label">Costo</label>
                                <input type="number" className="form-control" id="costo" name="costo" placeholder=""  value={costo} onChange={(e) =>{ setCosto(e.target.value)}} required/>
                            </div>
                        
                            <div className="mb-3">
                                <button type="button"  onClick={()=>{sendForm()}} className="btn btn-primary">Enviar</button>
                                <a href="/servicios" className="btn btn-secondary">Cancelar</a>
                            </div>
                        </form>
                    </div>
                ) : (<></>)
            }
           
        </>
    );
}

export default FormServices;