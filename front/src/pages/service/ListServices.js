import React, { useState , useEffect } from 'react';
import axios from 'axios';
import {URL_API} from '../../utils/constants';
import Preloader  from '../../components/Preloader';
const ListServices = () => {    
    const [showPreload,setShowPreload] = useState(false);
    useEffect(() => {
        getServicios();

      },[]);
    const [servicios,setServicios] = useState([]);


    const getServicios = async () => {
        setShowPreload(true);
        await axios.get(URL_API + 'servicios')
        .then(function (response) {
          // handle success
          setServicios(response.data);
          setShowPreload(false);
        })
        .catch(function (error) {
            setShowPreload(false);
          // handle error
          console.log(error);
        });
    }


    const eliminarRegistro = async (id , index) => {
        if(window.confirm("Se procederá a eliminar el servicios")){
            await axios.delete(URL_API + 'servicios/eliminar/' + id)
            .then(function (response) {
                if(response.status === 200){
                    const serviciosCopy = [...servicios]; 
                    alert(response.data);
                    serviciosCopy.splice(index, 1);
                    setServicios(serviciosCopy);  
                }
                //console.log(response);
                //getServicios(response.data);
            })
            .catch(function (error) {
                if(error.response.status === 400)
                alert('E' + error.response.status + ': ' + error.response.data);
                if(error.response.status === 500)
                    alert('Hay un problema con el servidor, volvé a intentar.');
            });
        }
    }

    return (
        <div className='container'>
            <div className="card">
                <div className="card-header">
                    Servicios <a href='/servicios/crear' type="button" className="btn btn-success">+</a>
                </div>
                <div className="card-body">
                <Preloader show={showPreload} />
                <table className='table table-striped'>
                    <thead className='thead-light'>
                        <tr>
                            <th>ID</th>
                            <th>Servicio</th>
                            <th>Costo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            servicios.map((servicio , index) => 
                            <tr key={index}>
                                <td >{servicio.id}</td>
                                <td >{servicio.name}</td>
                                <td >{servicio.cost}</td>
                                <td >
                                    <a href={'/servicios/' + servicio.id }  type="button" className="btn btn-warning">Editar</a>
                                    <button type="button" className="btn btn-danger" onClick={()=> eliminarRegistro(servicio.id , index)}>Eliminar</button>
                                </td>
                            </tr>
                            )
                        }
                    </tbody>
                        </table>
                </div>
                </div>
        </div>
    )
}

export default ListServices