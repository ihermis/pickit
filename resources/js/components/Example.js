import axios from 'axios';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

function Example() {
    const [marcas,setMarcas] = useState([]);
    const tokenLaravel = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    const configAxios = {
        headers: {
            'X-CSRF-TOKEN': tokenLaravel,
        }
    }
    useEffect(() => {
        //getMarcas();
        //console.log(tokenLaravel);
        //testSend();
      },[]);

    const getMarcas = async () => {
        await axios.get('/marca')
        .then(function (response) {
          // handle success
          console.log(response);
          setMarcas(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }

    const testSend = async () => {
        await axios.put('/clientes/3',
        {
            firstname: "Javier33333333333",
            lastname: "Antillano",
            _token : tokenLaravel
        }
        )
        .then(function (response) {
          // handle success
          console.log(response);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Example Component</div>

                        {/* <div className="card-body">
                            <select>
                                {
                                    marcas.map((marca , index) => 
                                    <option key={index}>{marca.name}</option>)
                                }
                            </select>
                        </div> */}

                        <div className="container">
                            <form action="/clientes/3" method="POST">
                            <input type="hidden" name="_token" value={tokenLaravel}></input>
                            <input type="hidden" name="_method" value="PUT"></input>
                                <div className="mb-3">
                                    <label htmlFor="nombre" className="form-label">Nombre</label>
                                    <input type="text" className="form-control" id="nombre" name="firstname" placeholder="" defaultValue="JavierAAAAAAAAAAAaaaa" />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="apellido" className="form-label">Apellido</label>
                                    <input type="text" className="form-control" id="apellido" name="lastname" placeholder="" defaultValue="Antillano" />
                                </div>
                            
                               <div className="mb-3">
                                    <button type="submit" className="btn btn-primary">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
