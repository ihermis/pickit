export const tokenLaravel = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
export const configAxios = {
    headers: {
        'X-CSRF-TOKEN': tokenLaravel,
    }
}