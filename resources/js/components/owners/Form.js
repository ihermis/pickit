import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { tokenLaravel, configAxios } from '../Utils';
import ReactDOM from 'react-dom';


function FormOwner(props) {
    const [title,setTitle] = useState("");
    const [showForm,setShowForm] = useState(false);
    const [showPreload,setShowPreload] = useState(true);
    const [firstname,setFirstname] = useState('Hermis');
    const [lastname,setLastname] = useState('2');
    const [methodForm,setMethodForm] = useState('');
    useEffect(() => {
        if(props.form === "crear"){
            setTitle("Nuevo cliente");
            setMethodForm('POST');
        }else{
            setMethodForm('PUT');
            setTitle("Editar cliente");
        }

        setTimeout(()=>{
                setShowPreload(false);
                setShowForm(true);
            },500);
        
       // sendForm();
       console.log(props);
      },[props]);
    
    const sendForm = async () => {
        //event.preventDefault();
        console.log({
            firstname: firstname,
            lastname: lastname,
            _method : methodForm
        });
        await axios.post('/clientes',
        {
            firstname: firstname,
            lastname: lastname,
            _method : methodForm
        },
        configAxios
        )
        .then(function (response) {
          // handle success
          console.log(response);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }

    return (
        <>
            {
                showPreload ? (
                    <div className='d-flex justify-content-center preload'>
                        <div className="spinner-border text-info" role="status">
                            <span className="sr-only"></span>
                        </div>
                    </div>
                ) : (<></>)
            }

            {
                showForm ? (
                    <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="/home" >Listado de Propietarios</a></li>
                            <li className="breadcrumb-item active" aria-current="page">{title}</li>
                        </ol>
                    </nav>                        
                    <h1>{title}</h1>
                    <form method="POST" onSubmit={()=>{sendForm()}}>
                        <input type="hidden" name="_token" value={tokenLaravel}></input>
                        <input type="hidden" name="_method" value={methodForm}></input>
                            <div className="mb-3">
                                <label htmlFor="nombre" className="form-label">Nombre</label>
                                <input type="text" className="form-control" id="nombre" name="firstname" placeholder="Juan" value={firstname} onChange={(e) =>{ setFirstname(e.target.value)}} required/>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="apellido" className="form-label">Apellido</label>
                                <input type="text" className="form-control" id="apellido" name="lastname" placeholder="Perez"  value={lastname} onChange={(e) =>{ setLastname(e.target.value)}} required/>
                            </div>
                        
                            <div className="mb-3">
                                <button type="submit" className="btn btn-primary">Enviar</button>
                            </div>
                        </form>
                    </div>
                ) : (<></>)
            }
           
        </>
    );
}

export default FormOwner;

if (document.getElementById('form_owner')) {
    const propsContainer = document.getElementById("form_owner");
    const props = Object.assign({}, propsContainer.dataset);
    ReactDOM.render(<FormOwner {...props}/>, document.getElementById('form_owner'));
}
