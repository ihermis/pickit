import axios from 'axios';
import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import Form from './Form';

function ListOwners() {
    const [propietarios,setPropietarios] = useState([]);
    const tokenLaravel = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    let configAxios = {
        headers: {
            'X-CSRF-TOKEN': tokenLaravel,
        }
      }
    useEffect(() => {
        getPropietarios();
        //console.log(tokenLaravel);
        //testSend();
      },[]);
    
    const [titulo,setTitulo] = useState("test");
    const [formDefault,setFormDefault] = useState("crear");
    const [showList,setShowList] = useState(true);
    const [showForm,setShowForm] = useState(false);
    const cambiar = () =>{
        setTitulo("test 2");
    }

    const getPropietarios = async () => {
        await axios.get('/clientes')
        .then(function (response) {
          // handle success
          console.log(response);
          setPropietarios(response.data);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }


    const testSend = async () => {
        await axios.put('/clientes/3',
        {
            firstname: "Javier33333333333",
            lastname: "Antillano",
            _token : tokenLaravel
        }
        )
        .then(function (response) {
          // handle success
          console.log(response);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        });
    }

    const crearNuevoRegistro = () => {
        setShowList(false);
        setFormDefault('crear');
        setShowForm(true);
    }

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Propietarios</div>
                        
                        {
                            showList ? (
                                <>
                                <button onClick={() => { crearNuevoRegistro();}}>cambiar</button>
                                <table className='table'>
                                <thead className='thead-light'>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>N° Autos</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        propietarios.map((propietario , index) => 
                                        <tr key={index}>
                                            <td >{propietario.id}</td>
                                            <td >{propietario.firstname}</td>
                                            <td >{propietario.lastname}</td>
                                            <td >12</td>
                                            <td >
                                                <button type="button" className="btn btn-warning">Editar</button>
                                                <button type="button" className="btn btn-danger">Eliminar</button>
                                            </td>
                                        </tr>
                                        )
                                    }
                                </tbody>
                        </table>
                        </>
                        ) : (<></>)
                        } 

                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListOwners;

if (document.getElementById('owner')) {
    ReactDOM.render(<ListOwners />, document.getElementById('owner'));
}
