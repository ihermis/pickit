<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\ModelCarController;
use App\Http\Controllers\BrandCarController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\CarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::resource('marcas', BrandCarController::class);
Route::resource('modelos', ModelCarController::class);


Route::group(['prefix' => 'clientes'], function() {
    Route::get('/', [OwnerController::class, 'index']);
    Route::get('/{id}', [OwnerController::class, 'show']);
    Route::post('/guardar', [OwnerController::class, 'store']);
    Route::put('/actualizar/{id}', [OwnerController::class, 'update']);
    Route::delete('/eliminar/{id}', [OwnerController::class, 'destroy']);
    });


Route::group(['prefix' => 'servicios'], function() {
    Route::get('/', [ServiceController::class, 'index']);
    Route::get('/{id}', [ServiceController::class, 'show']);
    Route::post('/guardar', [ServiceController::class, 'store']);
    Route::put('/actualizar/{id}', [ServiceController::class, 'update']);
    Route::delete('/eliminar/{id}', [ServiceController::class, 'destroy']);
});

Route::group(['prefix' => 'autos'], function() {
    Route::get('/', [CarController::class, 'index']);
    Route::get('/{id}', [CarController::class, 'show']);
    Route::get('/clientes/{id}', [CarController::class, 'carByOwner']);
    Route::post('/guardar', [CarController::class, 'store']);
    Route::put('/actualizar/{id}', [CarController::class, 'update']);
    Route::delete('/eliminar/{id}', [CarController::class, 'destroy']);
    Route::post('/agregar-presupuesto', [CarController::class, 'addBudget']);
});
